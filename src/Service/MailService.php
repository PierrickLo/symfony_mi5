<?php

namespace App\Service;


use App\Entity\Commande;
use Swift_Message;
use Twig\Environment;

class MailService
{

    /**
     * @var \Swift_Mailer
     */
    private $mailer;
    /**
     * @var Environment
     */
    private $environment;

    public function __construct(\Swift_Mailer $mailer, Environment $environment)
    {
        $this->mailer = $mailer;
        $this->environment = $environment;
    }

    public function sendMailConfirmation(Commande $commande)
    {
        $message = (new \Swift_Message('Confirmation de commande'))
            ->setFrom('admin@example.com')
            ->setTo($commande->getUsager()->getEmail())
            ->setBody(
                $this->environment->render('emails/confirmation.html.twig', [
                    'commande' => $commande
                ])
            );
        $this->mailer->send($message);
    }

}
