<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Entity\Produit;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/{_locale}/boutique")
 */
class BoutiqueController extends AbstractController
{

    /**
     * @Route("/", name="categorie")
     * @return
     */
    public function index()
    {
        $this->getDoctrine()->getManager()->flush();
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();

        return $this->render('boutique/categorie.html.twig', [
            'categories' => $categories,
            'nombreRayon' =>sizeof($categories),
        ]);

    }

    /**
     * @Route("/rayon/{id}", name="rayon")
     * @param int $id
     * @return Response
     */
    public function rayon(int $id){

        $produits = $this->getDoctrine()->getRepository(Produit::class)->findByCategorie($id);

        return $this->render('boutique/rayon.html.twig', [
            "produits" => $produits,
            "nombreProduit" =>sizeof($produits),
        ]);

    }

    /**
     * @Route("/recherche", name="recherche")
     * @param Request $request
     * @return Response
     */
    public function rechercher(Request $request)
    {
        $texte = $request->get('recherche');
        $produits = $this->getDoctrine()->getRepository(Produit::class)->findSearchProduct($texte);
        return $this->render('boutique/rayon.html.twig', [
            "produits" => $produits,
            "nombreProduit" =>sizeof($produits),

        ]);
    }

    public function plusVendu(){
        $liste = $this->getDoctrine()->getRepository(Produit::class)->findMostProductOrdered();

        return $this->render('boutique/plusVendu.html.twig', [
            "listPlusVendu" => $liste
        ]);
    }

}