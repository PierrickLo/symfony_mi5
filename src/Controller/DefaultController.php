<?php

namespace App\Controller;

use App\Service\PanierService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\BrowserKit\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/{_locale}/accueil")
 */
class DefaultController extends AbstractController
{

    /**
     * @Route("/", name="accueil")
     */
    public function index()
    {
        return $this->render('default/index.html.twig', [
        ]);
    }

    /**
     * @Route("/contact", name="contact")
     */
    public function contact()
    {
        $contact = "test";
        return $this->render('default/contact.html.twig', [
            'contact' => $contact,
        ]);

    }

    public function navbar(PanierService $panierService){
        $nbProduit = $panierService->getNbProduct();
        return $this->render('twig/navbar.html.twig', [
            'contact' => $nbProduit,
        ]);
    }
}