<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Entity\Commande;
use App\Entity\Usager;
use App\Form\UsagerType;
use App\Repository\UsagerRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/{_locale}/usager")
 */
class UsagerController extends AbstractController
{
    /**
     * @var SessionInterface
     */
    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * @Route("/", name="usager_index", methods={"GET"})
     * @param UsagerRepository $usagerRepository
     * @return Response
     * @Security("has_role('ROLE_ADMIN')")
     */

    public function index(UsagerRepository $usagerRepository): Response
    {
        // Index sera utilisé pour l'administrateur plus tard

        return $this->render('usager/index.html.twig', [
            'usager' => $this->getDoctrine()->getRepository(Usager::class)->findOneById($this->session->get('usager')),
        ]);
    }

    /**
     * @Route("/new", name="usager_new", methods={"GET","POST"})
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     */
    public function new(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $usager = new Usager();
        $form = $this->createForm(UsagerType::class, $usager);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            // Encryptage du mot de passe
            $usager->setPassword($passwordEncoder->encodePassword($usager,$usager->getPassword()));
            // Définition du rôle
            $usager->setRoles(["ROLE_CLIENT"]);
            $em->persist($usager);
            $em->flush();
            $this->session->set('usager', $usager->getId());
            $this->addFlash('success', 'Compte créé');

            return $this->redirectToRoute('app_login');
        }

        return $this->render('usager/new.html.twig', [
            'usager' => $usager,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/commande", name="commande", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function commandes(Request $request): Response
    {
        $commandes = $this->getDoctrine()->getRepository(Commande::class)->findBy(['usager' => $this->getUser()->getId()], ['date_commande' => 'DESC']);

        return $this->render('usager/commande.html.twig', [
            'usager' => $this->getUser(),
            'commandes' => $commandes,
        ]);
    }

    /**
     * @Route("/{id}", name="usager_show", methods={"GET","POST"})
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param Usager $usager
     * @return Response
     */
    public function show(Request $request, UserPasswordEncoderInterface $passwordEncoder, Usager $usager): Response
    {
        $form_edit = $this->createForm(UsagerType::class, $usager);
        $form_edit->remove('email');
        $form_edit->remove('password');
        $form_edit->handleRequest($request);

        if ($form_edit->isSubmitted() && $form_edit->isValid()) {
            $em = $this->getDoctrine()->getManager();
            // Encryptage du mot de passe
            $usager->setPassword($passwordEncoder->encodePassword($usager,$usager->getPassword()));
            // Définition du rôle
            $usager->setRoles(["ROLE_CLIENT"]);
            $em->persist($usager);
            $em->flush();
            $this->session->set('usager', $usager->getId());

            return $this->redirectToRoute('usager_show', ['id' => $usager->getId()]);
        }

        return $this->render('usager/show.html.twig', [
            'usager' => $usager,
            'form' => $form_edit->createView(),
        ]);
    }

}
