<?php

namespace App\Repository;

use App\Entity\Produit;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Produit|null find($id, $lockMode = null, $lockVersion = null)
 * @method Produit|null findOneBy(array $criteria, array $orderBy = null)
 * @method Produit[]    findAll()
 * @method Produit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProduitRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Produit::class);
    }


    public function findSearchProduct(string $texte)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.libelle LIKE :search')
            ->setParameter('search', '%'.$texte.'%')
            ->orWhere('p.texte LIKE :search')
            ->setParameter('search', '%'.$texte.'%')
            ->getQuery()
            ->getResult();
    }

    public function findMostProductOrdered(int $limit = 4)
    {
        $conn = $this->getEntityManager()->getConnection();
        // PROBLEME DE LIAISON ENTRE LES ENTITES VIA LES FICHIERS, LA BDD EST OK MAIS PAS LES FICHIERS, OBLIGER DE PASSER PAR UNE REQUETE EN DURE....
        // TODO Corriger la liaison pour faire un queryBuilder propre
        $sql = 'select p.id, p.visuel, p.libelle, SUM(l.quantite) as quantite from produit p, ligne_commande l WHERE p.id = l.produit_id GROUp BY p.libelle ORDER BY quantite DESC LIMIT 5';
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    // /**
    //  * @return Produit[] Returns an array of Produit objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Produit
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
